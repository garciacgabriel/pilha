Algumas das principais usabilidades utilizadas foram pop, push e clear.

A função pop() remove o último elemento do array e retorna o elemento.
A função push() adiciona um ou mais elementos ao final de um array e retorna o novo array.
A função clear() remove todos os elementos de um array.

As funções foram utilizadas para unico e exclusivamente, aprender sobre pilhas, seu conceito, e sua utilização.

O que é uma pilha?
É uma estrututa de dados simples, onde todas as alterações são feitas no topo (ou inicio) da lista.
